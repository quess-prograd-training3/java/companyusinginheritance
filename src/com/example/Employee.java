package com.example;

public class Employee {
    protected int empID;
    protected boolean passPort;
    protected boolean drivingLicence;
    protected int workHours;
    protected int extraHours;
    protected int nonPaidLeaves;
    protected int salary;

    public Employee(int empID, boolean passPort, boolean drivingLicence, int workHours, int extraHours, int nonPaidLeaves) {
        this.empID = empID;
        this.passPort = passPort;
        this.drivingLicence = drivingLicence;
        this.workHours = workHours;
        this.extraHours = extraHours;
        this.nonPaidLeaves = nonPaidLeaves;
    }


}
