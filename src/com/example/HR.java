package com.example;

public class HR extends Employee{
    public HR(int empID, boolean passPort, boolean drivingLicence, int workHours, int extraHours, int nonPaidLeaves) {
        super(empID, passPort, drivingLicence, workHours, extraHours, nonPaidLeaves);
    }

    public int calculateSalary(){
        int baseSalary=25000;
        int extraHoursSalary=this.extraHours*1000;
        int salaryDeduction=this.nonPaidLeaves*baseSalary/30;
        return salary= baseSalary+extraHoursSalary-salaryDeduction;
       // salary=(this.workHours+this.extraHours)*(29-this.nonPaidLeaves);
    }
}
